const tsp_service = require('./services/tsp-service');
const express = require('express');

const app = express();


app.get('/tsp', (req, res) => {
	const route = JSON.parse(req.query['route']);
	const tsp = new tsp_service();
	const result = tsp.calculate(route);
	res.send(JSON.stringify(result));
});


app.listen(3000, () => {
	console.log('escuchando puerto 3000');
});
