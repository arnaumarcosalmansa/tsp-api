const geolib = require('geolib');

class TspService {

	constructor() {
		this.optimalpath = [];
	}

	calculate(waypoints)
	{
		let minDistance = 0;
		this.optimalpath = [];
		for (let i = 0; i < waypoints.length; i++) {
			//el comienzo
			const waypoint = waypoints[i];
			//un array vacio
			const childPath = [];
			//todos los puntos menos el coienzo
			const otherWaypoints = waypoints.slice();
			otherWaypoints.splice(i, 1);

			let distance = this.travel(waypoint, otherWaypoints, childPath);
			if(i === 0 || distance < minDistance) {
				this.optimalpath = childPath.slice();
				this.optimalpath.push(waypoint);
				minDistance = distance;
			}
		}
		return this.optimalpath;
	}


	travel(startWaipoint, otherWaypoints, allWaypoints)
	{
		if (otherWaypoints.length === 1) {
			const lastWaypoint = otherWaypoints.shift();
			allWaypoints.push(lastWaypoint);
			return geolib.getDistance(startWaipoint, lastWaypoint);
		}

		const shortestPath = [];
		let minDistance = 0;
		for (let i = 0; i < otherWaypoints.length; i++) {
			const nextOtherWaypoints = otherWaypoints.slice();
			const nextStart = otherWaypoints[i];
			nextOtherWaypoints.splice(i, 1);

			const childPath = [];
			let distance = geolib.getDistance(startWaipoint, nextStart) + this.travel(nextStart, nextOtherWaypoints, childPath);
			if (i === 0 || distance < minDistance) {
				shortestPath.splice(0, shortestPath.length);
				shortestPath.push(...childPath);
				shortestPath.push(nextStart);
				minDistance = distance;
			}
		}
		allWaypoints.splice(0, allWaypoints.length);
		allWaypoints.push(...shortestPath);
		return minDistance;
	}
}

module.exports = TspService;